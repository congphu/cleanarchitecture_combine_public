//
//  MoviesViewModel.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation
import Combine


protocol MovieListViewModelInput {
    var didLoad: PassthroughSubject<Void, Never> { get }
}

protocol MovieListViewModelOutput {
    var isLoading: CurrentValueSubject<Bool, Never> { get }
    var errorString: PassthroughSubject<String, Never> { get }
    var listMovies: CurrentValueSubject<[MovieViewModel], Never> { get }
}

protocol MovieListViewModelInterface: MovieListViewModelOutput, MovieListViewModelInput {
}

final class MovieListViewModel: MovieListViewModelInterface {
    private var cancellables = Set<AnyCancellable>()
    private let moviesUseCase: MoviesUseCaseInterface
    
    let didLoad = PassthroughSubject<Void, Never>()
    let listMovies = CurrentValueSubject<[MovieViewModel], Never>([])
    let isLoading = CurrentValueSubject<Bool, Never>(false)
    let errorString = PassthroughSubject<String, Never>()
    
    init(moviesUseCase: MoviesUseCaseInterface) {
        self.moviesUseCase = moviesUseCase
        isLoading.value = true
        didLoad.flatMap({[unowned self] in self.moviesUseCase.getTrendingList()})
            .sinkToResult { [weak self] result in
                self?.isLoading.value = false
                switch result {
                case .success(let movies):
                    self?.listMovies.value = movies.map({MovieViewModel(entity: $0)})
                case .failure(let error):
                    switch error {
                    case .remoteData:
                        self?.errorString.send("There is something wrong with BE")
                    case .localData:
                        self?.errorString.send("There is something wrong with retrieving data from local")
                    default:
                        self?.errorString.send("There is something wrong with storing data")
                    }
                }
            }
            .store(in: &cancellables)
    }

}

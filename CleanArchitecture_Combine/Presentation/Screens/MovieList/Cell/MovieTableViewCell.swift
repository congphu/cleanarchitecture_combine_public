//
//  MovieTableViewCell.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import UIKit
import Combine
import SDWebImage

class MovieTableViewCell: UITableViewCell, NibProvidable, ReusableView {

    @IBOutlet private var title: UILabel!
    @IBOutlet private var subtitle: UILabel!
    @IBOutlet private var rating: UILabel!
    @IBOutlet private var poster: UIImageView!


    func bind(to viewModel: MovieViewModel) {
        title.text = viewModel.title
        subtitle.text = viewModel.releaseDate
        rating.text = viewModel.rating
        poster.sd_setImage(with: viewModel.poster, completed: nil)
    }
}

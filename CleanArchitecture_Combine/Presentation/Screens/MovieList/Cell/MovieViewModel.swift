//
//  MovieViewModel.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation
import UIKit.UIImage
import Combine

struct MovieViewModel {
    let id: Int
    let title: String?
    let releaseDate: String?
    let overview: String?
    var poster: URL?
    let rating: String

    init(id: Int, title: String?, releaseDate: String?, overview: String?, poster: String, rating: Double) {
        self.id = id
        self.title = title
        self.releaseDate = releaseDate
        self.overview = overview
        self.poster = ApiConstants.smallImageUrl.appendingPathComponent(poster)
        self.rating = rating.description
    }
    
    init(entity: MovieEntity) {
        self.id = entity.id
        self.title = entity.title
        if let releaseDate = entity.releaseDate {
            self.releaseDate = "\(NSLocalizedString("Release Date", comment: "")): \(dateFormatter.string(from: releaseDate))"
        } else {
            self.releaseDate = NSLocalizedString("To be announced", comment: "")
        }
        self.overview = entity.overview
        if let poster = entity.poster {
            self.poster = ApiConstants.smallImageUrl.appendingPathComponent(poster)
        }
        self.rating = entity.rating.description
    }
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
}

extension MovieViewModel: Hashable {
    static func == (lhs: MovieViewModel, rhs: MovieViewModel) -> Bool {
        return lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}


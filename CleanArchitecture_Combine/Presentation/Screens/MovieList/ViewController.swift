//
//  ViewController.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import UIKit
import Combine
import Resolver

class ViewController: UIViewController {
    private var cancellables = Set<AnyCancellable>()
    private let viewModel: MovieListViewModelInterface = Resolver.resolve()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupObservers()
        setupUI()
        viewModel.didLoad.send()
    }
    
    private func setupUI() {
        tableView.register(MovieTableViewCell.nib, forCellReuseIdentifier: MovieTableViewCell.reuseIdentifier)
        tableView.dataSource = self
    }
    
    private func setupObservers() {
        viewModel.isLoading
            .sink { [weak self] isLoading in
                if isLoading {
                    self?.loadingIndicator.startAnimating()
                    return
                }
                self?.loadingIndicator.stopAnimating()
            }
            .store(in: &cancellables)
        
        viewModel.errorString
            .sink { error in
                print(error)
            }
            .store(in: &cancellables)
        
        viewModel.listMovies
            .sink { [weak self] _ in
                self?.tableView.reloadData()
            }
            .store(in: &cancellables)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.listMovies.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.reuseIdentifier, for: indexPath) as! MovieTableViewCell
        cell.bind(to: viewModel.listMovies.value[indexPath.row])
        return cell
    }
}


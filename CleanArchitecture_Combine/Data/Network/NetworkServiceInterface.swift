//
//  NetworkServiceType.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 18/07/2022.
//

import Foundation
import Combine

protocol NetworkServiceInterface: AnyObject {
    func load<T: Decodable>(_ resource: EndPoint<T>) -> AnyPublisher<T, Error>
}

/// Defines the Network service errors.
enum NetworkError: Error {
    case invalidRequest
    case invalidResponse
    case dataLoadingError(statusCode: Int, data: Data)
    case jsonDecodingError(error: Error)
}

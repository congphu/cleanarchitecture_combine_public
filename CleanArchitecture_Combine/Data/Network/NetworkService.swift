//
//  NetworkService.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 18/07/2022.
//

import Foundation
import Combine

final class NetworkService: NetworkServiceInterface {
    private let session: URLSession

    init(session: URLSession = URLSession(configuration: URLSessionConfiguration.ephemeral)) {
        self.session = session
    }
    
    func load<T>(_ endPoint: EndPoint<T>) -> AnyPublisher<T, Error> {
        guard let request = endPoint.request else {
            return .fail(NetworkError.invalidRequest)
        }
        return session.dataTaskPublisher(for: request)
            .mapError { _ in NetworkError.invalidRequest }
            .print()
            .flatMap { data, response -> AnyPublisher<Data, Error> in
                guard let response = response as? HTTPURLResponse else {
                    return .fail(NetworkError.invalidResponse)
                }

                guard 200..<300 ~= response.statusCode else {
                    return .fail(NetworkError.dataLoadingError(statusCode: response.statusCode, data: data))
                }
                return .just(data)
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .subscribe(on: Scheduler.backgroundWorkScheduler)
            .eraseToAnyPublisher()
    }

}

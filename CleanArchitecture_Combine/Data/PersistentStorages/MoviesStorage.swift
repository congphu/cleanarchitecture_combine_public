//
//  MoviesStorage.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation
import Combine
import RealmSwift

protocol MovieStorageInterface {
    func getMovies() -> AnyPublisher<[MovieEntity], Error>
    func saveMovies(_ movies: [MovieModel]) -> AnyPublisher<Void, Error>
}

final class MovieRealmStorage: MovieStorageInterface {
    private let realm = try! Realm()

    func getMovies() -> AnyPublisher<[MovieEntity], Error> {
        Just.init(Array(realm.objects(MovieRealmModel.self)).map({$0.toDomain()}))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
    
    func saveMovies(_ movies: [MovieModel]) -> AnyPublisher<Void, Error> {
        Future { [weak self] promise in
            do {
                try self?.realm.write({
                    self?.realm.deleteAll()
                    self?.realm.add(movies.map({$0.toDbObject()}))
                })
                promise(.success(()))
            }
            catch {
                return promise(.failure(error))
            }
        }.eraseToAnyPublisher()
    }
}

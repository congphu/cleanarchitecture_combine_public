//
//  TrendingModel.swift
//  CodeChallenge
//
//  Created by cong on 18/02/2022.
//

import Foundation

// MARK: - Welcome
class TrendingList: Codable {
    let page: Int
    let results: [MovieModel]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

// MARK: - Result
class MovieModel: Codable {
    let originalTitle: String?
    let posterPath: String
    let video: Bool?
    let id: Int
    let overview: String
    let releaseDate: String?
    let voteCount: Int
    let adult: Bool?
    let backdropPath: String
    let voteAverage: Double
    let genreIDS: [Int]
    let title: String?
    let popularity: Double
    let mediaType: MediaType
    let name, originalName: String?
    let originCountry: [String]?
    let firstAirDate: String?
    

    enum CodingKeys: String, CodingKey {
        case originalTitle = "original_title"
        case posterPath = "poster_path"
        case video, id, overview
        case releaseDate = "release_date"
        case voteCount = "vote_count"
        case adult
        case backdropPath = "backdrop_path"
        case voteAverage = "vote_average"
        case genreIDS = "genre_ids"
        case title, popularity
        case mediaType = "media_type"
        case name
        case originalName = "original_name"
        case originCountry = "origin_country"
        case firstAirDate = "first_air_date"
    }
}

enum MediaType: String, Codable {
    case movie = "movie"
    case tv = "tv"
}

enum OriginalLanguage: String, Codable {
    case en = "en"
    case fr = "fr"
    case ja = "ja"
    case th = "th"
}

// MARK: - Mappings to Domain

extension MovieModel {
    func toDomain() -> MovieEntity {
        return .init(id: id, title: title, releaseDate: dateFormatter.date(from: releaseDate ?? ""), overview: overview, poster: posterPath, rating: voteAverage)
    }
}

// MARK: - Mappings to DB Object

extension MovieModel {
    func toDbObject() -> MovieRealmModel {
        return .init(from: self)
    }
}

 let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
}()


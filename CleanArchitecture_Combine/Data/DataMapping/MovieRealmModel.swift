//
//  MovieRealmModel.swift
//  CodeChallenge
//
//  Created by cong on 18/02/2022.
//
import Foundation
import RealmSwift

class MovieRealmModel: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String?
    @objc dynamic var releaseDate: String?
    @objc dynamic var overview: String?
    @objc dynamic var poster: String?
    @objc dynamic var rating: Double = 0
    
    override class func primaryKey() -> String? {
        "id"
    }
    
    convenience init(from model: MovieModel) {
        self.init()
        self.id = model.id
        self.title = model.title
        self.releaseDate = model.releaseDate
        self.poster = model.posterPath
        self.rating = model.voteAverage
        self.overview = model.overview
    }
}

// MARK: - Mappings to Domain

extension MovieRealmModel {
    func toDomain() -> MovieEntity {
        return .init(id: id, title: title, releaseDate: dateFormatter.date(from: releaseDate ?? ""), overview: overview, poster: poster, rating: rating)
    }
}

//
//  MoviesRepository.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation
import Combine

final class MovieRepository: MoviesRepositoryInterface {
    
    private let networkService: NetworkServiceInterface
    private let movieStorage: MovieStorageInterface
    
    init(networkService: NetworkServiceInterface, movieStorage: MovieStorageInterface) {
        self.networkService = networkService
        self.movieStorage = movieStorage
    }
    
    func getTrendingList() -> AnyPublisher<[MovieEntity], Error> {
        networkService.load(EndPoint<TrendingList>.movies())
            .receive(on: Scheduler.mainScheduler)
            .flatMap { [movieStorage] movieData in
            movieStorage.saveMovies(movieData.results).flatMap({ _ in Just.init(movieData.results.map({$0.toDomain()}))})
        }.eraseToAnyPublisher()
    }

    
    func getMoviesFromLocal() -> AnyPublisher<[MovieEntity], Error> {
        movieStorage.getMovies()
    }
}

extension EndPoint {
    static func movies() -> EndPoint {
        let url = ApiConstants.baseUrl.appendingPathComponent("/trending/all/day")
        let parameters: [String : CustomStringConvertible] = [
            "api_key": ApiConstants.apiKey,
            "language": Locale.preferredLanguages[0]
            ]
        return EndPoint(url: url, parameters: parameters)
    }
}

class MockMovieRepository: MoviesRepositoryInterface {
    func getTrendingList() -> AnyPublisher<[MovieEntity], Error> {
        let mockData = [MovieEntity(id: 0, title: "Mock Movie", releaseDate: Date(), overview: nil, poster: nil, rating: 5)]
        return Just(mockData).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
    
    func getMoviesFromLocal() -> AnyPublisher<[MovieEntity], Error> {
        return Just([]).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}

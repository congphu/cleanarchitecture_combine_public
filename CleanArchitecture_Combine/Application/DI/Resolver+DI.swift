//
//  Resolver+DI.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 18/07/2022.
//

import Foundation
import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        defaultScope = .graph
        registerNetworksAndStorages()
        registerRepositories()
        registerUseCases()
        registerViewModels()
    }
    
    static private func registerNetworksAndStorages() {
        register(NetworkServiceInterface.self) {
            NetworkService()
        }.scope(.application)
        
        register(MovieStorageInterface.self) {
            MovieRealmStorage()
        }.scope(.application)
    }
    
    static private func registerRepositories() {
        register(MoviesRepositoryInterface.self) {
            MovieRepository(networkService: resolve(), movieStorage: resolve())
        }
    }
    
    static private func registerUseCases() {
        register(MoviesUseCaseInterface.self) {
            MoviesUseCase(moviesRepository: resolve())
        }
    }
    
    static private func registerViewModels() {
        register(MovieListViewModelInterface.self) {
            MovieListViewModel(moviesUseCase: resolve())
        }
    }
    
}

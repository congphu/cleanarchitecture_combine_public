//
//  File.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation
import Combine

protocol MoviesRepositoryInterface {
    func getTrendingList() -> AnyPublisher<[MovieEntity], Error>
    func getMoviesFromLocal() -> AnyPublisher<[MovieEntity], Error>
}

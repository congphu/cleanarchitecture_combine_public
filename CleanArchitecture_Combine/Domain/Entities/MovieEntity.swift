//
//  MovieEntity.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation

struct MovieEntity {
    let id: Int
    let title: String?
    let releaseDate: Date?
    let overview: String?
    let poster: String?
    let rating: Double
}

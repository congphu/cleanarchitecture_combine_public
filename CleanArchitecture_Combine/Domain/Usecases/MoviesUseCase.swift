//
//  MoviesUseCase.swift
//  CleanArchitecture_Combine
//
//  Created by Cong Phu on 17/07/2022.
//

import Foundation
import Combine

enum MovieError: Error {
case remoteData
case savingData
case localData
}

protocol MoviesUseCaseInterface {
    func getTrendingList() -> AnyPublisher<[MovieEntity], MovieError>
}

class MoviesUseCase: MoviesUseCaseInterface {
    private let moviesRepository: MoviesRepositoryInterface
    
    init(moviesRepository: MoviesRepositoryInterface) {
        self.moviesRepository = moviesRepository
    }
    
    func getTrendingList() -> AnyPublisher<[MovieEntity], MovieError> {
        let localData = moviesRepository.getMoviesFromLocal().filter({!$0.isEmpty}).mapError({ _ in MovieError.localData })
        
        let remoteData = moviesRepository.getTrendingList().mapError({ _ in MovieError.remoteData })
        
        return Publishers.Merge(localData, remoteData).eraseToAnyPublisher()
    }
    
}
